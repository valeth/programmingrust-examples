extern crate iron;
#[macro_use]
extern crate mime;
extern crate router;
extern crate urlencoded;

mod gcd;
mod webserver;

fn main() {
    webserver::init(3001);
}