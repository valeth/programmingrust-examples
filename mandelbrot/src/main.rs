extern crate num;
extern crate image;
extern crate crossbeam;

mod parse;
mod render;
mod mandelbrot;

use std::io::Write;
use parse::{parse_complex, parse_pair};
use render::{render_threaded, write_image};

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 5 {
        writeln!(std::io::stderr(),
                 "Usage: {} FILE PIXELS UPPERLEFT LOWERRIGHT",
                 args[0]).unwrap();
        writeln!(std::io::stderr(),
                 "Example: {} mandel.png 1000x750 -1.20,0.35 -1,0.20",
                 args[0]).unwrap();
        std::process::exit(1);
    }

    let bounds = parse_pair(&args[2], 'x').expect("Error while parsing image dimensions");
    let upper_left = parse_complex(&args[3]).expect("Error while parsing upper left corner point");
    let lower_right = parse_complex(&args[4]).expect("Error while parsing lower right corner point");

    let mut pixels = vec![0; bounds.0 * bounds.1];
    render_threaded(&mut pixels, bounds, upper_left, lower_right);
    write_image(&args[1], &pixels, bounds).expect("Error while writing PNG image file");
}
